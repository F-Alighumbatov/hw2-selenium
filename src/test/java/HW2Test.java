import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class HW2Test {
    WebDriver webDriver;
    WebDriverWait wait;

    @BeforeEach
    void addInfo() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();

    }

    @Test
    void openNewTab() {
        webDriver.get("https://demoqa.com/browser-windows");
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/div[1]/div[1]/button")).click();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        ArrayList<String> windowList = new ArrayList<>(webDriver.getWindowHandles());
        webDriver.switchTo().window(windowList.get(1));
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement actual = webDriver.findElement(By.cssSelector("#sampleHeading"));
        String expected = "This is a sample page";
        assertEquals(expected, actual.getText());

    }

    @Test
    void iFrameCheck() {
        webDriver.get("https://demoqa.com/frames");
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.switchTo().frame("frame1");
        String actual = webDriver.findElement(By.xpath("/html/body/h1")).getText();
        String expected = "This is a sample page";
        assertEquals(expected, actual);

    }

    @Test
    void uploadDownloadTest() {
        webDriver.get("https://demoqa.com/upload-download");
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement downloadBtn = webDriver.findElement(By.linkText("Download"));
        downloadBtn.click();
        WebElement uploadBtn = webDriver.findElement(By.id("uploadFile"));
        uploadBtn.sendKeys("D:\\Hw2EndVersions\\Tasks"); // updated absolute path file
        WebElement uploadCheck = webDriver.findElement(By.id("uploadedFilePath"));
        assertTrue(uploadCheck.isDisplayed());

    }

    @Test
    void dropDownSelectCheking() throws AWTException {
        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        webDriver.get("https://www.phptravels.net/admin");

        WebElement loginField = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/div[1]/label[1]/input"));
        loginField.click();
        loginField.sendKeys("admin@phptravels.com");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/form[1]/div[1]/label[1]/input")));

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement paswordField = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/div[1]/label[2]/input"));
        paswordField.click();
        paswordField.sendKeys("demoadmin");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/form[1]/div[1]/label[2]/input")));


        WebElement loginButton = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/button"));
        loginButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.findElement(By.xpath("//*[@id=\"social-sidebar-menu\"]/li[3]")).click();
        WebElement hotelCheking = webDriver.findElement(By.xpath("//*[@id=\"checkedbox\"]"));
        if (!hotelCheking.isSelected()) {
            hotelCheking.click();
        }
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelsButton = webDriver.findElement(By.cssSelector("#social-sidebar-menu > li:nth-child(7) > a"));
        hotelsButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelsHOTELSSButton = webDriver.findElement(By.cssSelector("#Hotels > li:nth-child(1) > a"));
        hotelsHOTELSSButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelADDButton = webDriver.findElement(By.cssSelector("#content > div.panel.panel-default > form > button"));
        hotelADDButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelNameField = webDriver.findElement(By.cssSelector("#GENERAL > div:nth-child(2) > div > input"));
        hotelNameField.click();
        hotelNameField.sendKeys("Fairmont USA, Flame Towers");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#GENERAL > div:nth-child(2) > div > input")));

        Select statusEnable = new Select(webDriver.findElement(By.cssSelector("#content > form > div.col-md-4.sticky > div > div.panel-body.form-horizontal > div:nth-child(1) > div > select")));
        statusEnable.selectByIndex(0);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        Select starsSelect = new Select(webDriver.findElement(By.cssSelector("#content > form > div.col-md-4.sticky > div > div.panel-body.form-horizontal > div:nth-child(2) > div > select")));
        starsSelect.selectByIndex(4);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        Select hotelType = new Select(webDriver.findElement(By.cssSelector("#content > form > div.col-md-4.sticky > div > div.panel-body.form-horizontal > div:nth-child(3) > div > select")));
        hotelType.selectByIndex(1);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement locationBox = webDriver.findElement(By.xpath("//*[@id=\"s2id_searching\"]/a/span[2]"));

        locationBox.click();


        WebElement locationBoxInput = webDriver.findElement(By.cssSelector("#select2-drop > div > input"));
        locationBoxInput.sendKeys("Fairmont");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#select2-drop > ul > li:nth-child(1) > div")));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"select2-drop\"]/ul/li[1]")));
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);

        webDriver.switchTo().frame(0);
        WebElement textBox = webDriver.findElement(By.xpath("/html/body/p/br"));
        String description = "Located in the Flame Towers Complex, just a 3-minute walk from the famous Martyrs' " +
                "Alley, 5-star Fairmont Baku, Flame Towers features picturesque views of Baku City or Caspian Sea.\n" +
                "Modern air-conditioned rooms at Fairmont Baku, Flame Towers include contemporary décor " +
                "and a flat-screen TV. A spacious work desk, fully-stocked minibar and panoramic windows are also " +
                "fitted in all rooms. The marble bathrooms come with a glass wall and bidet. Guests are offered" +
                " rooms with iPod dock station and a coffee machine. Certain rooms offer access to Fairmont Gold " +
                "Lounge.\n" +
                "A rich variety of dishes is served in Fairmont's Le Bistro Restaurant featuring " +
                "International Bistro and Local cuisine. Original and traditional cocktails can be enjoyed at " +
                "Nur Lounge Bar. A selection of desserts as well as salads, sandwiches and sushi are offered at " +
                "Balcon Cafe.\n" +
                "With the phenomenal view of Caspian Sea, The Jazz Club seduce saxophone tunes nicely " +
                "orchestrated with piano melodies, on weekdays, for a cozy Jazz environment. On weekends, " +
                "talented DJ’s will set an amazing atmosphere all over the dance floor.";
        textBox.sendKeys(description);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/p")));
        webDriver.switchTo().defaultContent();

        WebElement submitButton = webDriver.findElement(By.cssSelector("#add"));
        submitButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#content > div.panel.panel-default > div.panel-body > div > div > div.xcrud-ajax")));
        String actual = "https://www.phptravels.net/admin/hotels/";
        String expected = webDriver.getCurrentUrl();
        assertEquals(actual, expected);
    }

    @Test
    void additionalElement() throws AWTException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        webDriver.get("https://www.phptravels.net/admin");

        WebElement loginField = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/div[1]/label[1]/input"));
        loginField.click();
        loginField.sendKeys("admin@phptravels.com");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/form[1]/div[1]/label[1]/input")));

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement paswordField = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/div[1]/label[2]/input"));
        paswordField.click();
        paswordField.sendKeys("demoadmin");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/form[1]/div[1]/label[2]/input")));


        WebElement loginButton = webDriver.findElement(By.xpath("/html/body/div[2]/form[1]/button"));
        loginButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.findElement(By.xpath("//*[@id=\"social-sidebar-menu\"]/li[3]")).click();
        WebElement hotelCheking = webDriver.findElement(By.xpath("//*[@id=\"checkedbox\"]"));
        if (!hotelCheking.isSelected()) {
            hotelCheking.click();
        }
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelsButton = webDriver.findElement(By.cssSelector("#social-sidebar-menu > li:nth-child(7) > a"));
        hotelsButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement hotelsHOTELSSButton = webDriver.findElement(By.cssSelector("#Hotels > li:nth-child(1) > a"));
        hotelsHOTELSSButton.click();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement selectAll = webDriver.findElement(By.xpath("/html/body/div[3]/div/div[2]/div[2]/div/div/div[1]/div[2]/table/thead/tr/th[1]/input"));

        selectAll.click();


        WebElement deleteSelected = webDriver.findElement(By.cssSelector("#deleteAll"));
        deleteSelected.click();

        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);

        Thread.sleep(2000);
        WebElement deleteAll = webDriver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div/div/div[1]/div[2]/table/tbody/tr/td[1]"));
        String actual = "Entries not found.";
        String expected = deleteAll.getText();
        assertEquals(actual, expected);

    }

    @AfterEach
    void endTest() {
        webDriver.quit();
    }

}