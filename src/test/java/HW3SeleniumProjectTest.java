import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HW3SeleniumProjectTest {
    WebDriver webDriver;
    WebDriverWait wait;

    @BeforeEach
    void addInfo() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();

    }

    @Test
    void task1Test() {
        webDriver.get("https://www.globalsqa.com/demo-site/draganddrop/");
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[@class='demo-frame lazyloaded']")));
        WebElement dragImage = webDriver.findElement(By.xpath("//*[@id='gallery']//img[contains(@alt,'The peaks of High Tatras')]"));
        WebElement dropImage = webDriver.findElement(By.xpath("//div[@id='trash']"));

        Actions builder = new Actions(webDriver);
        Action dropImageHT = builder.clickAndHold(dragImage).moveToElement(dropImage)
                .release(dropImage)
                .build();
        dropImageHT.perform();
        boolean expected = dragImage.isDisplayed();
        assertTrue(expected);

    }
    @Test
    void task2Test(){
        webDriver.get(" https://www.globalsqa.com/demo-site/select-dropdown-menu/");
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Select selectNumber = new Select(webDriver.findElement(By.xpath("//*[@id=\"post-2646\"]/div[2]/div/div/div/p/select")));
        List<WebElement> selectSize = selectNumber.getOptions();
        int actual = 249;
        int expected = selectSize.size();
        assertEquals(actual, expected);

    }
    @Test
    void task3Test() {
        webDriver.get(" https://www.globalsqa.com/samplepagetest/");
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement viberiteFayl = webDriver.findElement(By.cssSelector("#wpcf7-f2598-p2599-o1 > form > p > span > input"));
        viberiteFayl.sendKeys("D:\\HW3Selenium\\Hw3.txt"); //paste absolute path
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement nameField = webDriver.findElement(By.cssSelector("#g2599-name"));
        nameField.click();
        nameField.sendKeys("Farrux");
        WebElement emailField = webDriver.findElement(By.cssSelector("#g2599-email"));
        emailField.click();
        emailField.sendKeys("a.farrukh@outlook.com");
        Select experience = new Select(webDriver.findElement(By.cssSelector("#g2599-experienceinyears")));
        experience.selectByIndex(2);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement expertiseCheck = webDriver.findElement(By.xpath("//*[@id=\"contact-form-2599\"]/form/div[5]/label[2]/input"));
        expertiseCheck.click();
        WebElement educationCheck = webDriver.findElement(By.xpath("//*[@id=\"contact-form-2599\"]/form/div[6]/label[3]/input"));
        educationCheck.click();
        WebElement commentField = webDriver.findElement(By.xpath("//*[@id=\"contact-form-comment-g2599-comment\"]"));
        commentField.click();
        commentField.sendKeys("Hello World!!!");
        WebElement submitButton = webDriver.findElement(By.xpath("//*[@id=\"contact-form-2599\"]/form/p[3]/button"));
        submitButton.click();

        WebElement nameTest = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(1)"));
        String actualName = nameTest.getText();
        assertEquals(actualName,"Name: Farrux");

        WebElement emailTest = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(2)"));
        String actualEmail = emailTest.getText();
        assertEquals(actualEmail,"Email: a.farrukh@outlook.com");

        WebElement experienceTest = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(4)"));
        String actualExperience = experienceTest.getText();
        assertEquals(actualExperience,"Experience (In Years): 3-5");

        WebElement expertise = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(5)"));
        String actualExpertise = expertise.getText();
        assertEquals(actualExpertise,"Expertise :: Functional Testing");

        WebElement education = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(6)"));
        String actualEducation = education.getText();
        assertEquals(actualEducation,"Education: Post Graduate");

        WebElement comment = webDriver.findElement(By.cssSelector("#contact-form-2599 > blockquote > p:nth-child(7)"));
        String actualComment = comment.getText();
        assertEquals(actualComment,"Comment: Hello World!!!");


    }



    @AfterEach
    void endTest() {
        webDriver.quit();
    }


}